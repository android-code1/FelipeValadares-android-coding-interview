package com.beon.androidchallenge.domain.model

import com.beon.androidchallenge.data.repository.FactRepositoryImpl

interface FactRepository {

    fun getFactForNumber(number: String, callback: FactRepositoryImpl.FactRepositoryCallback<Fact>)
}