package com.beon.androidchallenge.data.repository

import com.beon.androidchallenge.domain.model.Fact
import com.beon.androidchallenge.domain.model.FactRepository
import javax.inject.Inject

class FactRepositoryImpl @Inject constructor (
    private var factRemoteDataSource: FactRemoteDataSource,
    private var factLocalDataSource: FactLocalDataSource
) : FactRepository {

    companion object {
        private var INSTANCE: FactRepositoryImpl? = null

        fun getInstance(): FactRepositoryImpl {
            if (INSTANCE == null) {
                INSTANCE = FactRepositoryImpl(FactRemoteDataSource(), FactLocalDataSource())
            }
            return INSTANCE!!
        }
    }

    override fun getFactForNumber(number: String, callback: FactRepositoryCallback<Fact>) {
        factRemoteDataSource.getFactForNumber(number,
            object : FactRemoteDataSource.GetFactForNumberCallback {
                override fun onFactLoaded(fact: Fact) {
                    callback.onResponse(fact)
                }

                override fun onFactLoadFailed() {
                    callback.onError()
                }
            })
    }

    interface FactRepositoryCallback<T> {
        fun onResponse(response: T)
        fun onError()
    }

}