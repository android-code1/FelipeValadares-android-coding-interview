package com.beon.androidchallenge.di

import com.beon.androidchallenge.data.repository.FactRepositoryImpl
import com.beon.androidchallenge.domain.model.FactRepository
import dagger.Binds
import dagger.Module
import dagger.hilt.InstallIn
import dagger.hilt.components.SingletonComponent

@Module
@InstallIn(SingletonComponent::class)
abstract class AppModule {

    @Binds
    abstract fun bindFactRepository(repositoryImpl: FactRepositoryImpl) : FactRepository
}