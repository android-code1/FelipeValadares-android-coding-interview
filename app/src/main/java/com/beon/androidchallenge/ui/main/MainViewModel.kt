package com.beon.androidchallenge.ui.main

import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import com.beon.androidchallenge.data.repository.FactRepositoryImpl
import com.beon.androidchallenge.domain.model.Fact
import com.beon.androidchallenge.domain.model.FactRepository
import javax.inject.Inject

class MainViewModel @Inject constructor(
    private val repository: FactRepository,
)  : ViewModel() {

    val currentFact = MutableLiveData<Fact?>(null)

    fun searchNumberFact(number: String) {
        if (number.isEmpty()) {
            currentFact.postValue(null)
            return
        }

        repository.getFactForNumber(number, object : FactRepositoryImpl.FactRepositoryCallback<Fact> {
            override fun onResponse(response: Fact) {
                currentFact.postValue(response)
            }

            override fun onError() {
                
            }

        })
    }
}